//Modules
//importing mongodb, mongoose and express
const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const app = express();
const path = require('path');

//Importing the schema for the Item List for the database
const Item = require('./Models/item');

//Connecting to MongoDB
const uri = "mongodb+srv://ninjaUser:bmwhqz029@cluster0.ugpfd.mongodb.net/ShoppingList?retryWrites=true&w=majority";
mongoose.connect(uri, {useNewUrlParser: true, useUnifiedTopology:true})
	//If a connecting is set and is found, then the server will start listening
	.then((result)=>{

		//Creating a listen in order to run the server on localhost:8080 or any port available depending on user
		const port = process.env.PORT || 8080;
		app.listen(port, () => console.log("Listening on port "+ port + "..."));

	})
	//Else the error is caught and logged
	.catch((err)=>console.log(err));


//npm install body-parser
const bodyParser = require('body-parser');

//Array to hold the list of item names
var itemsGlobalList = [];


//Using express to give access to all the static files in the public folder in order to be able to access the JS and CSS files already written
app.use(express.static('public'));
//This returns middleware that only parses json objects
app.use(bodyParser.json());

//Routes

//A get used to display the main HTML page to the user upon loading localHost, index.html being the main page.
app.get('/',function(req,res){
	//Sending the HTML file to the request made from the client
	res.sendFile('public/index.html', {root: path.join(__dirname, './')});

});

//A get used to display the create page when the client requests to create a new item
app.get('/createshoppingitemform', function(req, res){
	//Sending the createShoppingItemForm html page to the client
	res.sendFile('public/createShoppingItemForm.html', {root: path.join(__dirname, './')});
});

//A get similar to the one above however sending the update page instead of create, in order to update existing item
app.get('/updateshoppingitemform', function(req, res){
	//Sending the updateShoppingItemForm to the client
	res.sendFile('public/updateShoppingItemForm.html', {root: path.join(__dirname, './')});
});

//A route handler function for HTTP GET requests to the site with url ('/shopping') which is used to send the items in the list to the client
app.get('/shopping', function(req, res){
	//Sending a json response, sending batch the array containing the list of items
	res.json({items: itemsGlobalList});

});

//A route used to handle HTTP post requests used to create and push items to the array based on the parameter value
app.post('/shopping/item', function(req, res) {
//Gets the parameter value i.e. the new item name and pushes it to the array
	itemsGlobalList.push(req.body.item);
	//A json response is sent indicating that the item has been created
	res.json({Status: "Created"});

});

//A route used to handle HTTP put requests specifically to update an already existing item in the list
app.put('/shopping/item/:oldItem', function(req, res){
//The old item name is obtained from the parameter, same as the new item name to be added whose index is also obtained from the path
	var oldItemIndex = itemsGlobalList.indexOf(req.params.oldItem);
	//Provided the item exists in the array by checking that the index >=0, the item name is updated
	itemsGlobalList[oldItemIndex] = oldItemIndex >= 0
		? req.body.item
		: req.params.oldItem;
	//The response calls the send() function to indicate that the item has been successfully updated
	res.send("Updated");
});

//A route used to handle HTTP delete requests to delete a specific item from the list
app.delete('/shopping/item/:deleteItem', function(req, res){
//The index of the item to be deleted is obtained from the path received
	var deleteItemIndex = itemsGlobalList.indexOf(req.params.deleteItem);
	//If the index is >= 0, indicating that the item exists in the array, it is removed using the splice()
	if(deleteItemIndex >= 0)
		itemsGlobalList.splice(deleteItemIndex,1);
	res.send("Deleted");
});


//A post route used to handle post requests in order to add the item created to the database collection
app.post('/shopping/item/db', function(req, res) {
//Creating a new item of schema Item imported from the models folder
	const item = new Item({
//Setting item name to the name set in the parameter
		itemName: req.body.item
	});
	//Saving the database to persist the data catching any errors if any errors occur
	item.save()
		.then((result)=>{
			res.send(result)
		})
		.catch((err)=>{
			console.log(err);
		});
});

//A put route used to handle put requests in order to update an item in the database
app.put('/shopping/item/db/:oldItem', function(req, res){
//Obtaining the old and new item name from the provided data
	const oldItemName = req.params.oldItem;
	const newItem = req.body.item;

	//Used for deprecation purposes
	mongoose.set('useFindAndModify',false);

	//Using the findOneAndUpdate method provided by mongoose, in order to find an existing document
	Item.findOneAndUpdate({
		//Finding the document with the old name
		itemName: oldItemName
	}, {
		//Setting the new item name
			$set: {"itemName": newItem}
	})
		//If successful the Updated is sent to the client,else the error is caught
		.then(result=>{
			res.send("Updated");
		})
		.catch(err=>{console.log(err)});
});

//A route used to handle HTTP delete requests to delete a specific document from the database
app.delete('/shopping/item/db/:deleteItem', function(req, res){
	const deleteItem = req.params.deleteItem;

	//Using the find and delete method to delete the specified document matching the query
	Item.findOneAndDelete({itemName: deleteItem})
		.then(result=>{
			res.send("Deleted");
		})
		.catch(err=>{console.log(err)});
});

//A route handler function for HTTP GET requests to the site with url ('/db/shopping') which is used to send the documents in the database to the client
app.get('/db/shopping', function(req, res){
	//Sending the documents found in the database containing the item names
	Item.find({},{'_id':0})
		.then(result=>{
			res.send(result);
		})
		.catch(err=>{console.log(err)});

});