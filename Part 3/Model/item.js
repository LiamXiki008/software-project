//This is the schema used to create a document in the database

//Importing mongoose and declaring the schema
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Structure of the schema
const itemSchema = new Schema({
   //One field of name itemName and type string which is a required field
    itemName: {
        type:String,
        required:true
    }
},{timestamps:true});

//Exporting the model in order to allow accessibility to other files main app.js
const Item = mongoose.model('Item', itemSchema);
module.exports = Item;