//Var used to check if item list is to be displayed to the user
let isShoppingListVisible = false;

//Initialisation of click event listeners
$(document).ready(initializeEventListeners);

function initializeEventListeners(){
    addButtonClickEventListener();
    viewButtonClickEventListener();
    cancelButtonClickEventListener();
    createButtonClickEventListener();
    deleteButtonClickEventListener();
}


//Function used to listen for a click on buttons with id add(main create button)
function addButtonClickEventListener() {
    $("#add").click(function() {
    //When the button is clicked the user is redirected to the url below which indicates the createShoppingItemForm page, that being the createShoppingItemForm.html
        window.location.href = 'http://localhost:8080/createshoppingitemform';
        return false;
    });

}

//Function used to render the shopping list calling the function createItemNode for each item in the list in the server side
function renderShoppingList(parentNode, shoppingListArray){
//Creating the list by creating an item for each item in the list provided by the server and appending each item to the ol shoppingList
    shoppingListArray.forEach(shoppingListItem => {
        parentNode.append(createItemNode(shoppingListItem));

    });
}

//This function is used to create the list items
function createItemNode(shoppingListItem){
    //Creating a div, which will contain the item
    let div = document.createElement('div');
    //Creating a span which will contain the itemName
    let span = document.createElement('li');
    //Setting the span inner text to the item name provided as parameter
    span.innerText = shoppingListItem;
    //Creating an update button
    let updateButton = document.createElement('button');
    updateButton.innerText = "Update";
   //Creating a delete button
    let deleteButton = document.createElement('button');
    deleteButton.innerText = "Delete";

    //Appending the span, updateButton and delete button to the div
    div.append(span);
    div.append(updateButton);
    div.append(deleteButton);

    //Setting the id for the update and delete buttons to the itemName provided
    updateButton.setAttribute('id', shoppingListItem);
    deleteButton.setAttribute('id', shoppingListItem);

    //Adding the class list to the update and delete buttons for css purposes and click event listeners
    updateButton.classList.add("updateButton");
    deleteButton.classList.add("deleteButton");

    //Finally this will return the div created containing the item and buttons
    return div;
}

//Function used to listen for the view button to be clicked displaying/hiding the item list based on the variable declared at the top
function viewButtonClickEventListener() {

    $("#view").click(function () {
        //Getting the ol shopping list
        let parent = document.getElementById("shoppingList")
        //Checking if iShoppingListVisible is false, removing anything if it is preloaded, and setting its display to none in order to hide it
        if (isShoppingListVisible) {
            while(parent.firstChild){
                parent.firstChild.remove()
            }
            parent.style.display = "none";
            isShoppingListVisible = false;
        }else{
            //Else a get request is sent to the server in order to obtain the list items from the url provided
            $.get("http://localhost:8080/shopping", function(data){
                //Calling the function renderShoppingList used to create the list
                renderShoppingList(parent, data.items)
            });
            //Setting the style of the list to block in order to be visible
            parent.style.display = "block";
            isShoppingListVisible = true;
        }
    });
}


//Function used to listen for button clicks on buttons with id create, i.e the main addButton
function createButtonClickEventListener(){
    $("#create").click(function() {
        //Getting the item name found in the input field of the create item form
        let itemName = document.getElementById("createItem");
        //Input validation to check if anything is entered at all
        if(itemName.value === ''){
            //Upon invalid entry, an alert appears
            alert('Invalid entry')
        } else {
            //Sending data to the server using an HTTP POST request, by sending the item name as data with dataType json
            $.ajax({
                type: 'POST',
                url: '/shopping/item',
                data: JSON.stringify({"item": itemName.value}),

                //Upon success the user will be redirected to the main page
                success: function () {
                    window.location.href = 'http://localhost:8080';
                    return false;
                },
                contentType: "application/json",
                dataType: 'json'

            });

        }
    });
}

//Function used to delete an item from the list
function deleteButtonClickEventListener(){

    //Once a button with class name deleteButton is click this function will be performed
    $(document).on('click','.deleteButton', function() {
        //The id of the button clicked, which represents the item name, is obtained
        let itemName = this.id;

        //A delete request is sent to the server in order to delete the item from the list
        $.ajax({
            type: 'DELETE',
            //url used to specify the itemName to be deleted
            url: '/shopping/item/'+itemName,
            //Upon success an alert pops ups for the user to refresh the list to see the difference
            success: function(){
                alert("Refresh List To See Change");
            }
        });
    });

}
//Function used to listen for any clicks on the cancel button, which will redirect the user back to the main page
function cancelButtonClickEventListener(){
    $("#cancel").click(function() {
        window.location.href = 'http://localhost:8080';
        return false;
    });
}
