//Initialsing the click event listeners
$(document).ready(initializeEventListeners);

//Initialising the functions using above initialisation
function initializeEventListeners(){

    updateButtonClickEventListener();
    cancelButtonClickEventListener();
    confirmUpdateClickEventListener();
    updateFormField();
}


//Function used to await for a click on a button with class updateButton
function updateButtonClickEventListener() {

    //Upon click, the id of the button is obtained(that being name of item to be updated) and attached to the url
    $(document).on('click', '.updateButton', function () {
        let oldItem = this.id;
        //User is then redirected to the update shopping item form
        window.location.href = 'http://localhost:8080/updateshoppingitemform?oldItemField=' + oldItem;
    });
}

//Function used to fill in the field which holds the name of the item to be updated
function updateFormField(){
    //An if statement used to check if anything is appended to the url
    if (window.location.href.indexOf("updateshoppingitemform") > -1) {
        //Variable used to stored the url received
        var urlParams = new URLSearchParams(window.location.search);
        //The variable used to get the name of the item to be updated
        var oldItem = urlParams.get('oldItemField');
        //Updating the value of the old item field to the value specified above
        var oldItemField = document.getElementById("oldItemField");
        oldItemField.value = oldItem;
    }
}

//Function used to confirm the update made to the item name/ list
function confirmUpdateClickEventListener(){

    //Waiting for click of button with id 'confirm'
    $("#confirm").click(function () {

        //Getting the value of the new name from the input field with id 'newItem' and the old item from the first input field
        let newItem = document.getElementById("newItem");
        var oldItemName = document.getElementById("oldItemField").value;

        //Input validation for the new item name
        if(newItem.value === ''){
            alert('Invalid Item Name');
        } else {

            //A put ajax request used to send the oldItemName via the url and sending the newItem name through as payload
            $.ajax({
                type: 'PUT',
                url: '/shopping/item/' + oldItemName,
                data: JSON.stringify({"item": newItem.value}),
                //Upon success the user is redirected to the main page
                success: function () {
                    window.location.href = 'http://localhost:8080';
                    return false;
                },
                contentType: "application/json",
                dataType: 'json'
            });
            //Redirecting the user to the main page once ready
            window.location.href = 'http://localhost:8080';
            return false;
        }
    });


}
//Cancel button event listener with same implementation as the other js file
function cancelButtonClickEventListener(){
    $("#cancel").click(function() {
        window.location.href = 'http://localhost:8080';
        return false;
    });
}