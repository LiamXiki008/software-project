//Modules
//Constants that import and make use of express and the path name
const express = require('express');
const app = express();
const path = require('path');

//npm install body-parser
const bodyParser = require('body-parser');



//Array to hold the list of item names
var itemsGlobalList = ['Chocolate']


//Using express to give access to all the static files in the public folder in order to be able to access the JS and CSS files already written
app.use(express.static('public'));
//This returns middleware that only parses json objects
app.use(bodyParser.json());

//Routes

//A get used to display the main HTML page to the user upon loading localHost, index.html being the main page.
app.get('/',function(req,res){
	//Sending the HTML file to the request made from the client
    res.sendFile('public/index.html', {root: path.join(__dirname, './')});

});

//A get used to display the create page when the client requests to create a new item
app.get('/createshoppingitemform', function(req, res){
	//Sending the createShoppingItemForm html page to the client
	res.sendFile('public/createShoppingItemForm.html', {root: path.join(__dirname, './')});
});

//A get similar to the one above however sending the update page instead of create, in order to update existing item
app.get('/updateshoppingitemform', function(req, res){
	//Sending the updateShoppingItemForm to the client
	res.sendFile('public/updateShoppingItemForm.html', {root: path.join(__dirname, './')});
});

//A route handler function for HTTP GET requests to the site with url ('/shopping') which is used to send the items in the list to the client
app.get('/shopping', function(req, res){
	//Sending a json response, sending batch the array containing the list of items
	res.json({items: itemsGlobalList});

});

//A route used to handle HTTP post requests used to create and push items to the array based on the parameter value
app.post('/shopping/item', function(req, res) {
//Gets the parameter value i.e. the new item name and pushes it to the array
	itemsGlobalList.push(req.body.item);
	//A json response is sent indicating that the item has been created
	res.json({Status: "Created"});

});

//A route used to handle HTTP put requests specifically to update an already existing item in the list
app.put('/shopping/item/:oldItem', function(req, res){
//The old item name is obtained from the parameter, same as the new item name to be added whose index is also obtained from the path
	var oldItemIndex = itemsGlobalList.indexOf(req.params.oldItem);
	//Provided the item exists in the array by checking that the index >=0, the item name is updated
	itemsGlobalList[oldItemIndex] = oldItemIndex >= 0
		? req.body.item 
		: req.params.oldItem;
	//The response calls the send() function to indicate that the item has been successfully updated
	res.send("Updated");
});

//A route used to handle HTTP delete requests to delete a specific item from the list
app.delete('/shopping/item/:deleteItem', function(req, res){
//The index of the item to be deleted is obtained from the path received
	var deleteItemIndex = itemsGlobalList.indexOf(req.params.deleteItem);
	//If the index is >= 0, indicating that the item exists in the array, it is removed using the splice()
	if(deleteItemIndex >= 0)
		itemsGlobalList.splice(deleteItemIndex,1);
	res.send("Deleted");
});


//The port variable used to allocate the port that we will be listening for requests from, port available depending on the user or 8080, which is the one I used
const port = process.env.PORT || 8080;
app.listen(port, () => console.log("Listening on port "+ port + "..."));
